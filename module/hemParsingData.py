# 
# Line parser for HEM data
# Parsing HEM data line by line 
# HEM has a file 'data.csv' which has voltage, power, current etc. data
# This example shows you how to parse and extract the content you want 

# Author: Ashray Manur

#Sample data in the file
#VOLT:13.50,NODE:0,DATE:12/10/17-6:57:30 - this line gives you voltage for node 0
#AMP:0.40,NODE:3,DATE:12/10/17-6:57:30 - this line gives you current for node 3
#POW:5.42,NODE:3,DATE:12/10/17-6:57:30 - this line gives you power for node 3

#THis module takes a line VOLT:13.50,NODE:0,DATE:12/10/17-6:57:30 and returns a dict like below

# {
# 	'type' : VOLT
# 	'value' : 13.65
# 	'node': 0
# 	'date': 2017-12-08 02:28:40
# }



import sys 
import os
import datetime
from dateutil import parser


def lineParser(line):

	data = line.split(",")
	#print data

	#data will look something like this
	# ['VOLT:13.65', 'NODE:0', 'DATE:12/8/17-2:29:7\n']

	#data is a list.
	# See all components of that list for each line by doing the following
	#print data[0] #'VOLT:13.65'
	#print data[1] #'NODE:0'
	#print data[2] #'DATE:12/8/17-2:29:7\n'

	#Now if we want to know if its power or volt or something else 

	dataType = data[0].split(':');
	#print dataType[0] # VOLT

	#Now if we want to the value of that type
	#print dataType[1] # 13.65

	#Now if we want the node number
	nodeNumber = data[1].split(':')
	#print nodeNumber[1] # 0

	#Now if we want to extract the date
	dateData_all = data[2].split(':', 1)
	dateData = dateData_all[1].split('\n')
	#print dateData_all #['DATE', '12/8/17-2:28:40']
	#print dateData #['12/8/17-2:28:40']

	#Now we need to get the date into a format Python understands well

	dateData_format = parser.parse(dateData[0]);
	#print dateData_format #2017-12-08 02:28:40

	parseObj = {'type': dataType[0], 'value': dataType[1], 'node': nodeNumber[1], 'date': dateData_format}

	return parseObj

