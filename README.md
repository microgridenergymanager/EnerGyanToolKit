
`module` - contains hem modules necessary to create an app. Copy this folder to your directory when creating your app. Any file starting with `hem` is a module.

`examples` - contains sample apps. 

----

Sample Apps

`bare.py` - boiler plate code for app 

`actuate.py` -  Turn on/off nodes through API

`demandManage.py` - Perform load management when power consumption crosses threshold

`sendEmail.py` - Send an email from your app
